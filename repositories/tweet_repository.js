var fs = require('fs');

function TweetRepository(adapter) {
    this._adapter = adapter;

    class Tweet {
        constructor(user, message) {
          this.user = user;
          this.message = message;
        }
    }

    TweetRepository.prototype.getTweets = function() {
        var tweets = [];
        const items = this._adapter.getItems();
        for (var i=0; i<items.length; i++) {
            const tweetLine = items[i];
            const tweetComponents = tweetLine.split('>');
            tweets.push(new Tweet(tweetComponents[0].trim(), tweetComponents[1].trim()));
        }
        return tweets;
    };

    TweetRepository.prototype.getList = function() {
        const tweets = this.getTweets();
        return tweets;
    };

    TweetRepository.prototype.getTweetsForUsers = function(users) {
        const tweets = this.getTweets();
        const tweetsOfUsers = tweets.filter(function(tweet){
            return users.includes(tweet.user);
        });
        return tweetsOfUsers;
    };
}

module.exports = TweetRepository;