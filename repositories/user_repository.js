var fs = require('fs');

function UserRepository(adapter) {
    this._adapter = adapter;

    var arrayOfUsersFromLine = function(line) {
        var lineString = line.trim();
        var usersCsv = lineString.replace("follows", ",").replace(/\s/g, "").trim();
        return usersCsv.split(",");
    };

    UserRepository.prototype.getList = function() {
        var usersSet = new Set();

        const items = this._adapter.getItems();
        for(var lineIndex=0; lineIndex<items.length; lineIndex++) {
            const lineOfUsers = items[lineIndex];
            const users = arrayOfUsersFromLine(lineOfUsers);
            for (var userOnLineIndex=0; userOnLineIndex<users.length; userOnLineIndex++) {
                var user = users[userOnLineIndex];
                usersSet.add(user);
            }
        }

        var uniqueUsersArray = Array.from(usersSet);
        return uniqueUsersArray.sort();
    };

    UserRepository.prototype.getFriendsOfUser = function(user) {
        var usersFriendsMap = new Map();

        const items = this._adapter.getItems();
        for (var lineIndex=0; lineIndex<items.length; lineIndex++) {
            const lineOfUsers = items[lineIndex];
            // console.log(`lineOfUsers: ${lineOfUsers}`);
            const users = arrayOfUsersFromLine(lineOfUsers);
            const user = users[0];

            var knownFriendsSet = usersFriendsMap.get(user);
            if (!knownFriendsSet) {
                knownFriendsSet = new Set();
            }

            // a user is technically also his own friend ;-)
            for (var i=0; i<users.length; i++) {
                var friend = users[i];
                knownFriendsSet.add(friend);
            }        
            
            usersFriendsMap.set(user, knownFriendsSet);
        }

        const friendsOfUser = usersFriendsMap.get(user);
        var uniqueUsersArray = (friendsOfUser)?Array.from(friendsOfUser):[];
        return uniqueUsersArray.sort();
    };
}

module.exports = UserRepository;