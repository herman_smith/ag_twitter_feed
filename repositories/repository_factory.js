const FileAdapter = require("../adapters/file_adapter");
const UserRepository = require("./user_repository");
const TweetRepository = require("./tweet_repository");

function RepositoryFactory() {

    const repositories = [
        {title: "user", instance: new UserRepository(new FileAdapter("./data/user.txt"))},
        {title: "tweet", instance: new TweetRepository(new FileAdapter("./data/tweet.txt"))}
    ];

    var find = function(repo, title) { 
        return repo.title === title;
    };

    RepositoryFactory.prototype.getRepositoryByTitle = function(title) {
        var match = repositories.find(function(repo) {return repo.title === title;});
        if (match) {
            return match.instance;
        } else {
            return undefined;
        }
    };
}

module.exports = RepositoryFactory;