const fs = require('fs');

function FileAdapter(filePath) {    
    var contentRead = fs.readFileSync(filePath, 'utf8').trim();
    this._items = contentRead.split(/\r?\n/);
}

FileAdapter.prototype.getItems = function() {
	return this._items;
};

module.exports = FileAdapter;