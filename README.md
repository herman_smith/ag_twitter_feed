# Twitter feed simulation

## Overview
This program simulates a twitter-like feed. The repositories currently abstract away their seven-bit ASCII file datasources. The UserRepository fronts a list of users and their followers. The TweetRepository does so for tweets. 

Given the users, followers and tweets, the objective is to display a simulated twitter feed for each user to the console

## Prerequisites
Have a version of Node.js >= 6.10.3 installed

## Running the application
To run the application:
```
node index.js
```
## Running the unit tests
To run the applications unit tests:
```
first run `npm install` (which should install the node modules "chai" and "mocha")

now you can execute `npm run test` in the root directory to run unit tests
```
