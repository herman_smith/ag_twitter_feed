var chai = require('chai');
var expect = chai.expect;

const RepositoryFactory = require('../repositories/repository_factory');
const UserRepository = require('../repositories/user_repository');

describe('RepositoryFactory', function () {

    var repositoryFactory = new RepositoryFactory();
    
    it('getRepositoryByTitle is a function', function () {
        console.assert(typeof repositoryFactory.getRepositoryByTitle === 'function');
    });

    it('getRepositoryByTitle should not return a repository for a non existing key', function(){
        expect(repositoryFactory.getRepositoryByTitle('kabaal')).to.equals(undefined);
    });

    it('getRepositoryByTitle should be able to return a user repository', function(){
        expect(repositoryFactory.getRepositoryByTitle('user')).to.instanceOf(UserRepository);
    });
});