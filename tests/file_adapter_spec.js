var chai = require('chai');
var expect = chai.expect;

const FileAdapter = require('../adapters/file_adapter');

describe('FileAdapter', function () {
    
    var fileAdapter = new FileAdapter("./data/user.txt");     

    it('getItems is a function', function () {
        console.assert(typeof fileAdapter.getItems === 'function');
    });
});