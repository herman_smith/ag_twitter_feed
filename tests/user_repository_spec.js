var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;

const UserRepository = require('../repositories/user_repository');

describe('UserRepository', function () {
    
    var mockAdapter = {
        getItems: function() { return ["Ward follows Alan", "Alan follows Martin", "Ward follows Martin, Alan", "Martin follows Steve, Alan, Frank"];},
    };
    var userRepository = new UserRepository(mockAdapter);

    it('getList is a function', function () {
        assert(typeof userRepository.getList === 'function');
    });

    it('getList returns a list with the same length as unique names in the mockAdapter', function () {
        assert.strictEqual(userRepository.getList().length, 5);
    });

    it('getFriendsOfUser is a function', function () {
        assert(typeof userRepository.getFriendsOfUser === 'function');
    });

    it('getFriendsOfUser returns a list with the same length as unique names of friends in the mockAdapter', function () {
        assert.strictEqual(userRepository.getFriendsOfUser('Alan').length, 2);
    });
});