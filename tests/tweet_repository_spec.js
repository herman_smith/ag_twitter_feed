var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;

const TweetRepository = require('../repositories/tweet_repository');

describe('TweetRepository', function () {
    
    var mockAdapter = {
        getItems: function() { return ["Linus> Those that can, do. Those that can't, complain.", "Seymoure> I just bought a Mac to help me design the next Cray.", "Eric> Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.", "Watson> I think there is a world market for about five computers.", "Linus> An infinite number of monkeys typing into GNU emacs would never make a good program.", "Campbell> Computers are like Old Testament gods; lots of rules and no mercy."];},
    };
    var tweetRepository = new TweetRepository(mockAdapter);

    it('getTweets is a function', function () {
        assert(typeof tweetRepository.getTweets === 'function');
    });

    it('getTweets returns a list with the same length as the number of tweets in the mockAdapter', function () {
        assert.strictEqual(tweetRepository.getTweets().length, 6);
    });

    it('getList is a function', function () {
        assert(typeof tweetRepository.getList === 'function');
    });

    it('getList returns a list with the same length as the number of tweets in the mockAdapter', function () {
        assert.strictEqual(tweetRepository.getList().length, 6);
    });

    it('getTweetsForUsers is a function', function () {
        assert(typeof tweetRepository.getTweetsForUsers === 'function');
    });

    it('getTweetsForUsers returns a list with the same length as tweets for that user in the mockAdapter (This user has no tweets)', function () {
        assert.strictEqual(tweetRepository.getTweetsForUsers(['Alan']).length, 0);
    });

    it('getTweetsForUsers returns a list with the same length as tweets for that user in the mockAdapter', function () {
        assert.strictEqual(tweetRepository.getTweetsForUsers(['Linus']).length, 2);
    });
});