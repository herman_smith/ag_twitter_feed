const RepositoryFactory = require("./repositories/repository_factory");

var repositoryFactory = new RepositoryFactory();
const userRepo = repositoryFactory.getRepositoryByTitle('user');
const tweetRepo = repositoryFactory.getRepositoryByTitle('tweet');
if (userRepo && tweetRepo) {
    listUsers(userRepo.getList());
}

function listUsers(alphabeticallyOrderedUsers) {
    for (var i=0; i<alphabeticallyOrderedUsers.length; i++) {
        const user = alphabeticallyOrderedUsers[i];
        const friends = userRepo.getFriendsOfUser(user);
        listTweetsOfFriends(user, friends);
    }
}

function listTweetsOfFriends(user, friends) {
    console.log(`${user}`);
    const tweets = tweetRepo.getTweetsForUsers(friends);
    for (var i=0; i<tweets.length; i++) {
        const tweet = tweets[i];
        console.log(`\t@${tweet.user}: ${tweet.message}`);
    }
}
